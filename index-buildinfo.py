#!/usr/bin/python3
from debian import deb822
from debian.deb822 import PkgRelation
import os
import sys
import psycopg2
import argparse
from parse_source import parse_source
from arches import Arches

def arch_string(str):
    if str:
        return str
    else:
        return 'any'

class BuildinfoDB:
    # index builds only from these arches.
    COVERED_ARCHES=None

    def __init__(self, verbose=0, incremental=False):

        self.verbose=verbose
        self.incremental=incremental

        self.conn = psycopg2.connect('dbname=buildinfo')
        self.cursor = self.conn.cursor()
        self.count = 0
        self.total = 0
        
        self.cursor.execute('''CREATE TABLE IF NOT EXISTS builds
                (id SERIAL PRIMARY KEY, origin text, path text, source text, source_version debversion,
                ''' + Arches.COLUMNS + ''',
                version debversion, build_date timestamp with time zone)''')

        self.cursor.execute('''CREATE TABLE IF NOT EXISTS depends
                (id INTEGER REFERENCES builds(id), depend text, arch debian_arch,
                 version debversion,
                 PRIMARY KEY (id,depend,arch))''')

        if self.incremental:
            self.cursor.execute('SELECT id,path from builds')
            self.path_cache = { row[1]: row[0] for row in self.cursor }
            if self.verbose > 0:
                print('caching {:d} paths'.format(len(self.path_cache)))

    def truncate(self):
        self.cursor.execute('TRUNCATE builds RESTART IDENTITY CASCADE')
        
    def index_one_file(self, path):

        self.total += 1
        if (self.total % 1000) == 0:
            if self.verbose > 0:
                print('{:d}/{:d} processed'.format(self.count,self.total))

        path_parts = path.split("/")[1:]
        trimmed_path = '/'.join(path_parts)

        with open(path) as contents:
            try:
                data = deb822.Deb822(contents)
                origin = data.get('Build-Origin')

                source_text = data.get('Source')
                (source_name, source_version) = parse_source(source_text)
                version = data.get('Version')
                source_version = source_version or version

                arches = data.get('Architecture')
                build_date = data.get('Build-Date')

                built_arches = {}
                index_this_buildinfo = False
                if BuildinfoDB.COVERED_ARCHES == None:
                    with open("arches.txt") as f:
                        BuildinfoDB.COVERED_ARCHES=[ x.rstrip() for x in f ]

                for arch in arches.split(" "):
                    built_arches[arch]=True
                    if arch in BuildinfoDB.COVERED_ARCHES:
                        index_this_buildinfo = True

                if index_this_buildinfo:
                    if self.incremental and self.path_cache.get(trimmed_path):
                        return

                    self.count += 1
                    if (self.count % 10000) == 0:
                        self.conn.commit()

                    arch_values=[]
                    for arch in Arches.RELEASE:
                        arch_values.append(arch in built_arches)

                    values=(origin, trimmed_path, source_name, source_version,
                                    *arch_values,  version, build_date)
                    self.cursor.execute('INSERT INTO builds VALUES (DEFAULT,%s,%s,%s,%s,%s,'+Arches.FORMAT+',%s) RETURNING id', values)
                    id = self.cursor.fetchone()[0]
                    depends=PkgRelation.parse_relations(data['Installed-Build-Depends'])
                    for dep in depends:
                        self.cursor.execute("INSERT INTO depends VALUES (%s,%s,%s,%s)",
                                       (id,dep[0]['name'],arch_string(dep[0]['archqual']),dep[0]['version'][1]))
            except KeyboardInterrupt:
                raise
            except:
                if self.verbose >= 0:
                    print(sys.exc_info()[0])
                    print("Failed to index "+path+". Continuing.")

    def index_from_log(self,line):
        if line[0:2] == '>f':
            (_, path) = line.split(' ')
            self.index_one_file(os.path.join(self.input_root,path))
        
    def close(self):
        self.conn.commit()
        self.conn.close()

parser = argparse.ArgumentParser()
parser.add_argument('-F', '--file-list', nargs='?', type=argparse.FileType('r'), default=None, const=sys.stdin,
                    help='(file containing) list of files to index')

parser.add_argument('-C', '--change-directory', help='change directory before indexing')

parser.add_argument('--verbose', '-v', action='count', default=0)

parser.add_argument('--quiet', '-q', action='count', default=0)

parser.add_argument('-i','--incremental', nargs='?', const=True, type=bool,
                    help='Avoid indexing the same path twice')

args = parser.parse_args()

args.verbose = args.verbose - args.quiet

full_index = not args.file_list
if args.change_directory:
    os.chdir(args.change_directory)

db = BuildinfoDB(verbose=args.verbose,incremental=args.incremental)

if full_index:
    if not args.incremental:
        db.truncate()
    for root, dirs, files in os.walk('buildinfo',followlinks=True):
        for path in [os.path.join(root,name) for name in files]:
            db.index_one_file(path)
else:
    for line in args.file_list:
        db.index_one_file(line.rstrip())
            
db.close()            




