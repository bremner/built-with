#!/bin/bash
# vim: set noexpandtab:

# originally rsync2buildinfos.debian.net, by Holger.
# Copyright 2019 Holger Levsen <holger@layer-acht.org>
# released under the GPLv=2

###################################################################
###								###
### this is running on coccia.debian.org via  crontab:          ###
###								###
### # m h  dom mon dow   command				###
### 0 */2 * * *  ~/bin/push-buildinfo                           ###
###								###
###################################################################

YEAR="$(date -u +%Y)"
MONTH="$(date -u +%m)"
DAY="$(date -u +%d)"
TARGETUSER="buildinfo"
TARGETHOST="buildinfo"
TARGETDIR="builtin-pho/buildinfo/Debian"
SOURCEDIR="/srv/ftp-master.debian.org/buildinfo/"
LOG="$HOME/rsync-$YEAR-$MONTH-$DAY.log"

rsync_day(){
	echo "$(date -u) - rsyncing .buildinfo files for $YEAR/$MONTH/$DAY..." >> $LOG
	rsync -e ssh -av $YEAR/$MONTH/$DAY $TARGETUSER@$TARGETHOST:$TARGETDIR/$YEAR/$MONTH/ >> $LOG 2>&1
}

rsync_month(){
	echo "$(date -u) - rsyncing .buildinfo files for $YEAR/$MONTH..." >> $LOG
	rsync -e ssh -av $YEAR/$MONTH $TARGETUSER@$TARGETHOST:$TARGETDIR/$YEAR/ >> $LOG 2>&1
}

rsync_year(){
	echo "$(date -u) - rsyncing .buildinfo files for $YEAR..." >> $LOG
	rsync -e ssh -av $YEAR $TARGETUSER@$TARGETHOST:$TARGETDIR/ >> $LOG 2>&1
}

#
# main
#
cd $SOURCEDIR
if [ -n "$1" ] ; then
	for i in $(seq 2016 $YEAR) ; do
		rsync_year
	done
else
	if [ "$DAY" = "01" ] && [ "$MONTH" = "01" ] ; then
		# even though we do this several times a day
		# it's ok, because the year is young :)
		rsync_year
	elif [ "$DAY" = "01" ] ; then
		# see comment above
		rsync_month
	else
		rsync_day
	fi
	# rsync yesterday, always
	YEAR="$(date -u -d '1 day ago' +%Y)"
	MONTH="$(date -u -d '1 day ago' +%m)"
	DAY="$(date -u -d '1 day ago' +%d)"
	rsync_day
	echo "======================================================================" >> $LOG
	# output yesterdays logfile if this hasn't been done yet
	OLDLOG="$HOME/rsync-$YEAR-$MONTH-$DAY.log"
	if [ -f "$OLDLOG" ] ; then
		cat $OLDLOG
		echo "$(date -u) - $OLDLOG flushed via cron output." >> $LOG
		rm $OLDLOG
	fi
fi
