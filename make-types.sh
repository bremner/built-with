#!/bin/sh

psql buildinfo <<EOF
DROP TYPE IF EXISTS debian_arch CASCADE;
CREATE TYPE
              debian_arch
       AS ENUM ('any', 'all', 'amd64', 'arm64', 'armel', 'armhf',
                'hurd-i386', 'i386',
                'kfreebsd-amd64', 'kfreebsd-i386', 'mips',
                'mips64el', 'mipsel', 'powerpc', 'ppc64el',
                'source', 's390x' );
EOF
