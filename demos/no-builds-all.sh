#!/bin/sh
# find binary packages with no buildinfo
psql buildinfo <<EOF
select distinct p.source,p.version
from
      binary_packages p
where
      p.suite='sid'
      and p.arch='all'
except
        select p.source,p.version
from binary_packages p, builds b
where
      b.source=p.source
      and p.source_version=b.source_version
EOF
