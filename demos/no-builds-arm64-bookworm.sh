#!/bin/sh
# find binary packages with no buildinfo
psql buildinfo <<EOF
select distinct p.package,p.source,p.source_version
from
      binary_packages p
where
      p.suite='bookworm'
      and p.arch='arm64'
except
        select p.package,p.source,p.source_version
from binary_packages p, builds b
where
      b.source=p.source
      and p.source_version=b.source_version
      and b.arch_arm64=True
EOF
